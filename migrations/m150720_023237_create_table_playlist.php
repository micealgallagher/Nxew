<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_023237_create_table_playlist extends Migration
{
    public function up()
    {
        $this->createTable('playlist_track', [
            'id' => Schema::TYPE_PK,
            'account_id' => Schema::integer(),
            'title' => Schema::string(500),
            'genre' => Schema::string(500),
            'artwork_url' => Schema::text(500),
            'permalink_uri' => Schema::string(500),
            'waveform_url' => Schema::string(500),
            'stream_url' => Schema::string(500),
            'uri' => Schema::string(500),
            'user_uri' => Schema::string(500),
            'user_username' => Schema::string(500),
            'user_permalink_url' => Schema::string(500),
            'user_avatar_url' => Schema::string(500),
        ]);

        $this->execute('ALTER TABLE `playlist_track` ADD CONSTRAINT `FK_PLAYLIST_TRACK_USER` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;');

        // Add default administrative user
        $this->insert('playlist_track', [
             'id' => 204088322,
             'account_id' => 1,
             'title' => 'Canteen Killa – Slo Breeze',
             'genre' => 'Futurebeat',
             'artwork_url' => 'https://i1.sndcdn.com/artworks-000115047423-zgo2qb-large.jpg',
             'permalink_uri' => 'http://soundcloud.com/pdrecs/canteen-killa-slo-breeze',
             'waveform_url' => 'https://w1.sndcdn.com/KPgR8o2oINIa_m.png',
             'stream_url' => 'https://api.soundcloud.com/tracks/183260985/stream',
             'uri' => 'https://api.soundcloud.com/tracks/183260985',
             'user_uri' => 'https://api.soundcloud.com/users/2009402',
             'user_username' => 'Pop Drone',
             'user_permalink_url' => 'http://soundcloud.com/pdrecs',
             'user_avatar_url' => 'https://i1.sndcdn.com/avatars-000089601818-2k2xm8-large.jpg'
         ]);

        $this->insert('playlist_track', [
             'id' => 204088323,
             'account_id' => 1,
             'title' => '1001 Nights',
             'genre' => 'maybeimselfish?',
             'artwork_url' => 'https://i1.sndcdn.com/artworks-000118792027-jnbrax-large.jpg',
             'permalink_uri' => 'http://soundcloud.com/ateller/1001-nights',
             'waveform_url' => 'https://w1.sndcdn.com/4OJ85wKqgtAX_m.png',
             'stream_url' => 'https://api.soundcloud.com/tracks/208259268/stream',
             'uri' => 'https://api.soundcloud.com/tracks/208259268',
             'user_uri' => 'https://api.soundcloud.com/users/80735946',
             'user_username' => '?TELLER',
             'user_permalink_url' => 'http://soundcloud.com/ateller',
             'user_avatar_url' => 'https://i1.sndcdn.com/avatars-000155404644-gxl3pk-large.jpg'
         ]);


        $this->insert('playlist_track', [
             'id' => 204088324,
             'account_id' => 1,
             'title' => 'Scattered Clouds - People Walk (Absoluut\'s Spectral Remix)',
             'genre' => 'Remix',
             'artwork_url' => 'https://i1.sndcdn.com/artworks-000065177491-7f9n4x-large.jpg',
             'permalink_uri' => 'http://soundcloud.com/pdrecs/scattered-clouds-people-walk',
             'waveform_url' => 'https://w1.sndcdn.com/cimvzbEwEfkz_m.png',
             'stream_url' => 'https://api.soundcloud.com/tracks/124623318/stream',
             'uri' => 'https://api.soundcloud.com/tracks/124623318',
             'user_uri' => 'https://api.soundcloud.com/users/2009402',
             'user_username' => 'Pop Drone',
             'user_permalink_url' => 'http://soundcloud.com/pdrecs',
             'user_avatar_url' => 'https://i1.sndcdn.com/avatars-000089601818-2k2xm8-large.jpg'
         ]);

        $this->insert('playlist_track', [
             'id' => 204088329,
             'account_id' => 1,
             'title' => 'Trance',
             'genre' => 'Javi Santiago',
             'artwork_url' => 'https://i1.sndcdn.com/artworks-000106547192-5f12x6-large.jpg',
             'permalink_uri' => 'http://soundcloud.com/javisantiagokeys/trance',
             'waveform_url' => 'https://w1.sndcdn.com/l5N82qe9IVWC_m.png',
             'stream_url' => 'https://api.soundcloud.com/tracks/190884281/stream',
             'uri' => 'https://api.soundcloud.com/tracks/190884281',
             'user_uri' => 'https://api.soundcloud.com/users/2415790',
             'user_username' => 'Javi.Santiago',
             'user_permalink_url' => 'http://soundcloud.com/javisantiagokeys',
             'user_avatar_url' => 'https://i1.sndcdn.com/avatars-000130709249-a4vezq-large.jpg'
        ]);
    }

    public function down()
    {
        echo "m150720_023237_create_table_playlist cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
