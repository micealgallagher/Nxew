<?php

use yii\db\Schema;
use yii\db\Migration;

class m160817_140957_create_setting_table extends Migration
{
    public function up()
    {
        $this->createTable('setting', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::string(200),
            'value' => Schema::text(). ' NOT NULL'
        ]);

        $this->insert('setting', [
            'name' => 'contribute_text',
            'value' => 'Nxew.ca is currently in Ottawa-only beta mode. As we expand into new cities across Canada we\'ll be looking for playlist contributions from people making music or making music happen. Please be in touch. Let us know where you\'re at and what music you\'d like to share with nxew.ca listeners.\nPlaylists on nxew.ca have 16 tracks each. When you add a new track up top, track 16 is removed automatically. there\'s no obligation to post a fresh playlist weekly or even monthly. The idea is when you hear new music - good shit you just can\'t get off your speakers - you drop that track into your playlist for others to hear.'
        ]);
    }

    public function down()
    {
        echo "m160817_140957_create_setting_table cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
