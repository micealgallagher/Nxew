<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_012512_create_table_account extends Migration
{
    public function up()
    {
        $this->createTable('account', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::integer(),
            'bio' => Schema::text() . ' NOT NULL',
            'avatar_url' => Schema::string(500),
            'website' => Schema::string(500),
            'facebook' => Schema::string(500),
            'twitter' => Schema::string(500),
            'soundcloud' => Schema::string(500),
            'playlist_name' => Schema::string(500),
        ]);

                // Add default administrative user
        $this->insert('account', [
            'user_id' => 1,
            'bio' => 'Proin quis lectus neque. Nullam aliquam volutpat lectus ac elementum. Sed odio lorem, mattis vitae viverra sit amet, aliquet non ex. Mauris a maximus arcu. Aenean convallis est quis urna sollicitudin gravida. Morbi pretium lacinia gravida. Etiam imperdiet quis lorem ac faucibus.',
            'avatar_url' => '',
            'website' => 'http://about.mehual.me',
            'facebook' => 'http://facebook.com',
            'twitter' => 'http://twitter.com',
            'soundcloud' => 'http://soundcloud.com',
            'playlist_name' => 'Top hits from the Ottawa valley'
        ]);

        $this->insert('account', [
            'user_id' => 2,
            'bio' => 'Et voluptatem qui dignissimos. Dolorum est ex vel aut iusto. Molestias est omnis aliquam consequatur. Sit expedita sed qui quasi vel. Ratione sit neque accusamus debitis veritatis.',
            'avatar_url' => '',
            'website' => 'http://about.mehual.me',
            'facebook' => 'http://facebook.com',
            'twitter' => 'http://twitter.com',
            'soundcloud' => 'http://soundcloud.com',
            'playlist_name' => 'Best we\'ve seen live in Ottawa'
        ]);

        $this->insert('account', [
            'user_id' => 3,
            'bio' => 'Dolorum consectetur sed maxime voluptatum corrupti dolores rerum qui. Ipsum dicta sapiente ut error dolorum sed et. Voluptatum quidem error commodi quo voluptatibus impedit ipsa atque. Dolores sint eaque qui facilis sed eligendi explicabo.',
            'avatar_url' => '',
            'website' => 'http://about.mehual.me',
            'facebook' => 'http://facebook.com',
            'twitter' => 'http://twitter.com',
            'soundcloud' => 'http://soundcloud.com',
            'playlist_name' => 'Best from the Ottawa circuit'
        ]);

        $this->insert('account', [
            'user_id' => 4,
            'bio' => 'Quae totam quisquam maxime totam quibusdam. Ipsum mollitia eveniet et. Quam et dolorem iure debitis deserunt. Et consequatur eum consequatur aut aut et in.',
            'avatar_url' => '',
            'website' => 'http://about.mehual.me',
            'facebook' => 'http://facebook.com',
            'twitter' => 'http://twitter.com',
            'soundcloud' => 'http://soundcloud.com',
            'playlist_name' => 'Best in the east'
        ]);

        $this->insert('account', [
            'user_id' => 5,
            'bio' => 'Quos voluptates harum in in commodi est. Voluptatem sint et sunt occaecati ut voluptatum neque. Consequatur qui pariatur ut ut vitae. Vitae et tenetur eveniet et vero.',
            'avatar_url' => '',
            'website' => 'http://about.mehual.me',
            'facebook' => 'http://facebook.com',
            'twitter' => 'http://twitter.com',
            'soundcloud' => 'http://soundcloud.com',
            'playlist_name' => 'Smokin\' hits'
        ]);
    }

    public function down()
    {
        echo "m150720_012512_create_table_account cannot be reverted.\n";

        return false;
    }
}
