<?php

use yii\db\Schema;
use yii\db\Migration;

class m150720_011441_create_table_user extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => Schema::TYPE_PK,
            'artist' => Schema::string(60) . ' NOT NULL',
            'auth_key' => Schema::string(100),
            'password_hash' => Schema::string(100),
            'password_reset_token' => Schema::string(100),
            'email' => Schema::string(500),
            'status' => Schema::smallInteger(),
            'type' => Schema::string(50) . ' NOT NULL',
            'created_at' => Schema::integer(). ' NOT NULL',
            'updated_at' => Schema::integer(). ' NOT NULL',
        ]);

        // Add default administrative user
        $this->insert('user', [
            'artist' => 'Infinite Loop',
            'auth_key' => 'Dril__yzXmVirSvtF48kYMi_60xFK3m2',
            'password_hash' => '$2y$13$0yDNUsECYzn33BGkycbUJ.B6G3IGWb74mSO5JGvv2ObwFSO9hHVx2',
            'password_reset_token' => '',
            'email' => 'me@mehaul.me',
            'type' => 'Administrator',
        ]);

        $this->insert('user', [
            'artist' => 'Smooshie on the hill',
            'auth_key' => 'Dril__yzXmVirSvtF48kYMi_60xFK3m2',
            'password_hash' => '$2y$13$0yDNUsECYzn33BGkycbUJ.B6G3IGWb74mSO5JGvv2ObwFSO9hHVx2',
            'password_reset_token' => '',
            'email' => 'one@one.com',
            'type' => 'User',
        ]);

        $this->insert('user', [
            'artist' => 'Bethandal',
            'auth_key' => 'Dril__yzXmVirSvtF48kYMi_60xFK3m2',
            'password_hash' => '$2y$13$0yDNUsECYzn33BGkycbUJ.B6G3IGWb74mSO5JGvv2ObwFSO9hHVx2',
            'password_reset_token' => '',
            'email' => 'two@two.com',
            'type' => 'User',
        ]);

        $this->insert('user', [
            'artist' => 'Kahifaner',
            'auth_key' => 'Dril__yzXmVirSvtF48kYMi_60xFK3m2',
            'password_hash' => '$2y$13$0yDNUsECYzn33BGkycbUJ.B6G3IGWb74mSO5JGvv2ObwFSO9hHVx2',
            'password_reset_token' => '',
            'email' => 'three@three.com',
            'type' => 'User',
        ]);

        $this->insert('user', [
            'artist' => 'Small Soliders',
            'auth_key' => 'Dril__yzXmVirSvtF48kYMi_60xFK3m2',
            'password_hash' => '$2y$13$0yDNUsECYzn33BGkycbUJ.B6G3IGWb74mSO5JGvv2ObwFSO9hHVx2',
            'password_reset_token' => '',
            'email' => 'four@four.com',
            'type' => 'User',
        ]);
    }

    public function down()
    {
        echo "m150720_011441_create_table_user cannot be reverted.\n";

        return false;
    }
}
