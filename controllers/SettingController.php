<?php

namespace app\controllers;

use app\models\AddTrackSoundCloudForm;
use app\models\PlaylistTrack;
use app\models\SecurityForm;
use app\models\User;
use app\models\Setting;
use Yii;
use app\models\Account;
use app\models\AccountSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Html;
use app\common\Constant;

/**
 * AccountController implements the CRUD actions for Account model.
 */
class SettingController extends Controller
{
   function actionUpdate($name) {
   		$model = Setting::findByName($name);
   		$model->load(Yii::$app->request->post());

        $model->save();
        
    	return $this->redirect(['account/view']);
   }
}