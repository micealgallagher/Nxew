<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
?>

<div class="row center-block" >

        <div class="contribute-panel col-xs-12 col-lg-8 col-lg-offset-2" style="margin-top: 100px; border: 0px solid black">

            <div class="col-xs-12 col-lg-6">
                <div class="panel">
                    <h2 class="panel-body title">North by East West</h2>
                    <div class="panel-body contribute-text">
                        <?= nl2br($setting->value) ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-lg-6">
                <div class="panel contribute-login-panel">
                    <div class="panel-body">
                    <?= $this->render('_form',[
                        'model' => $model
                    ]) ?>

                    </div>
                </div>
            </div>

        </div>

</div>