<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */


?>
<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
<?= $form->field($model, 'email')->textInput(['placeholder'=>'EMAIL', 'class' =>'form-control login-password'])->label('') ?>
<?= $form->field($model, 'password')->passwordInput(['placeholder'=>'PASSWORD', 'class' =>'form-control login-password'])->label('') ?>
<div class="form-group">
    <?= Html::submitButton('CONTRIBUTE', ['class' => 'btn login-button', 'name' => 'login-button', 'style' => 'width: 100%']) ?>
</div>
<div style="margin:1em 0; color: #b1b2ad; text-align: center">
    Did you <?= Html::a('forget your password', ['site/request-password-reset'], ['style' => 'color: #747a7a']) ?>?
</div>
<?php ActiveForm::end(); ?>