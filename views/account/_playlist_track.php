<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="panel-body playlist-panel-body">
    <?php
        if ( isset($playlistTracks) && sizeof($playlistTracks) > 0) {
            $trackUrls = '';
            $trackIndex = 0;
            for ($i = 0; $i < sizeof($playlistTracks); $i++ ) {
                $track = $playlistTracks[$i];
                $artworkUrl = $track->artwork_url;
                $title = $track->title;
                $user = $track->user_username;
                $genre = $track->genre;

                $trackUrls .= '"' . $track->permalink_uri . '",';

                $oddOrEvenClass = $i % 2 ? 'playlist-track-odd' : 'playlist-track-even';

    ?>

    <div class="row <?= $oddOrEvenClass ?>" track-container-id="<?= $trackIndex ?>">
        <div class="col-xs-6 col-xs-offset-5 col-md-2 col-md-offset-0 col-lg-2 col-lg-offset-0 coverart-container">
            
             <div class="track-coverart-container">
                <?= Html::img($artworkUrl) ?>
                <div class="no-pause-overlay" coverart-index="<?= $trackIndex ?>">
                    <i class="fa fa-pause fa-2x"></i>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-md-8 col-lg-8 playlist-track-details">
            <?= Html::tag('strong', $title) ?>
            <?= Html::tag('p', $user) ?>
        </div>
        <div class="col-xs-12 col-md-2 col-lg-2 col-lg-offset-0 <?= Yii::$app->user->isGuest ? "playlist-track-controls-basic" : "playlist-track-controls"?>">
            <div class="row">
                    
                <div style="" class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-4 col-sm-6 col-sm-offset-5 text-center track-controls">
                    <i class="fa fa-play-circle fa-2x control-hover" track-index="<?= $trackIndex++ ?>"></i>

                    <?php
                        if ( !Yii::$app->user->isGuest ) {
                            echo "<i class=\"fa fa-thumbs-up fa-2x\"></i>";
                        }
                    ?>                 
                </div>
                <div style="" class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-4 col-sm-6 col-sm-offset-5 text-center track-controls">
                    <?php 

                            $options = ['class' => 'fa fa-download fa-2x']; 
                            $icon = Html::tag('i', '', $options);

                            echo Html::a($icon, $track->permalink_uri);

                        if ( $doesUserOwnPlaylist  ) {
                            $options = ['class' => 'track-delete fa fa-trash-o fa-2x'];
                        
                            $icon = Html::tag('i', '', $options);
                            
                            $href = Url::toRoute([
                                'playlist/delete', 
                                'accountId' => $account->id,
                                'trackId' => $track->id
                            ]);

                            echo Html::a($icon, $href, 
                                ['class' => 'track-delete-link',
                                'track-title' => $track->title
                            ]);
                        }
                    ?>
                </div>



            </div>
        </div>
    </div>

    <?php
            }

            if ( isset($trackUrls) ) {
                echo Html::script('var trackUrls = [' . $trackUrls . ']');
            }
        } else {
            echo '<div class="account-bio" style="margin:20px; font-style: italic; text-align: center;" >';
                echo '<i class="fa fa-frown-o" aria-hidden="true"></i> where\'s the music?';
            echo '</div>';

        }
    ?>

</div>