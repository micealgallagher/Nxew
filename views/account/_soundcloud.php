<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div id="divAddSoundCloudDropDown" class="dropdown pull-right">

    <?php

        if ( isset($maxNumberOfTracksReached) && $maxNumberOfTracksReached ) {
            $buttonOptions = [
                'class' => 'add-track-button', 
                'data-toggle' => 'modal', 
                'data-target' => '.bs-example-modal-sm'
            ];
            $iconOptions = ['class' => 'fa fa-plus fa-1x'];
            $icon = Html::img(Yii::$app->request->baseUrl . '/img/icon-add.png');

            echo Html::button($icon, $buttonOptions);
        } else {
            ?>

            <button class="add-track-button dropdown-toggle" type="button" id="addSoundCloudTrack" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?=  Html::img(Yii::$app->request->baseUrl . '/img/icon-add.png') ?>
            </button>

            <ul class="dropdown-menu" aria-labelledby="addSoundCloudTrack" style="padding: 20px">
                <div id="divSoundCouldUrl" style="display:none" class="alert alert-danger" role="alert"></div>
                <li>
                    <div class="input-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-soundcloud"></i>
                            </span>
                            <input type="text" id="txtSoundCloudURL" class="form-control" placeholder="SOUNDCLOUD TRACK URL" style="height: 48px; width: 300px" />
                            <span class="input-group-btn">
                                <?php $options = ['class' => 'fa fa-plus'] ?>
                                <?= $icon = Html::tag('i', '', $options) ?>
                                <?= Html::script('var addToPlaylistUrl = \'' . Url::toRoute('playlist/add') . '\'') ?>
                                <?= Html::a($icon, '', ['onclick' => 'return resolveAndSubmitSCUrl(' . $account->id . ')', 'class'=>'btn btn-success', 'style' => 'height: 48px; width: 48px; padding-top: 25%']) ?>
                            </span>
                        </div>
                    </div>
                </li>
            </ul>
        <?php } ?>
</div>