<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */


?>
<div class="panel panel-success panel-shadow playlist-panel">
    <div class="panel-heading">
        <h3 class="account-panel-title" style="height: 30px">
            <span class="shadow-text" style="font-weight: bold;">
                Playlist
            </span>
            <span style="display: block; float left; font-size:small; padding-top: 5px; font-weight: normal">
                <?= $account->playlist_name ?>
            </span>

            <?php
                if ( $doesUserOwnPlaylist ) {
                    echo $this->render('_soundcloud', [
                        'account' => $account,
                        'maxNumberOfTracksReached' => $maxNumberOfTracksReached
                    ]);
                }
            ?>

        </h3>
    </div>

    <?= $this->render('_playlist_track', [
        'account' => $account,
        'maxNumberOfTracksReached' => $maxNumberOfTracksReached,
        'playlistTracks' => $playlistTracks,
        'doesUserOwnPlaylist' => $doesUserOwnPlaylist
    ]) ?>

    <?= $this->render('_max_track_alert', [
        'playlistTracks' => $playlistTracks
    ]) ?>
    
</div>