<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Update account';
?>
<div class="account-create">

<h1>&nbsp;</h1>

    <div class="row" id="nav">
        <div class="col-sm-2 col-md-2" id="account-menu">
        <?php

			$allMenuItems =[];
			array_push($allMenuItems, [
	            'label' => 'Profile',
	            'url' => ['account/update', 'id' => $model->id, '#' => 'profile']
	        ]);
        	array_push($allMenuItems, [
	            'label' => 'Playlist',
	            'url' => ['account/update', 'id' => $model->id, '#' => 'playlist']
	        ]);

	        if ( $user->isAdministrator() ) {
	        	array_push($allMenuItems, [
	            	'label' => 'Setting',
	            	'url' => ['account/update', 'id' => $model->id, '#' => 'setting']
	        	]);
	        }

			echo Nav::widget([
				    'items' => $allMenuItems,
			]);
		    ?>
        </div>
        <div class="col-sm-10 col-md-10">
        	<?php if ( (isset($isUpdated) ? $isUpdated : false) ) {
			    $options = ['class' =>'alert alert-success'];
			    echo Html::tag('div', '<strong>Updated!</strong> Bio updated successfully.', $options);
			}?>

			<?php if ( (isset($isSaved) ? $isSaved : false) ) {
			    $options = ['class' =>'alert alert-success'];
			    echo Html::tag('div', '<strong>Saved!</strong> Bio created successfully.', $options);
			}?>

			<?= $this->render('_form', [
			    'model' => $model,
			    'user' => $user,
			    'contributeSetting' => $contributeSetting,
			]) ?>
        </div>
    </div>

</div>
<script>

</script>