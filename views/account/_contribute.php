<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


?>
<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">
            <a name="setting" id="playlist">Setting</a>
        </h3>
            <?php
                $form = ActiveForm::begin(['id' => 'update-setting', 'action' => Yii::$app->getUrlManager()->createUrl(['setting/update', 'name' => $contributeSetting->name])]);
                
                $options = ['class' => 'btn pull-right accept-bio-button'];
                $icon = Html::tag('i', '', $options);

                echo Html::submitButton($icon, ['class' => 'pull-right accept-bio-button']);
            ?>
    </div>
    <div class="panel-body" style='max-height: 200px'>
        
        <?= $form->field($contributeSetting, 'value')
                ->textArea([
                    'rows' => '6',
                    'placeholder' => 'CONTRIBUTE PAGE TEXT',
                    'class' => 'form-control login-password'
                ])->label('Contribute Page Text') ?>
        <?= $form->field($contributeSetting, 'name')->hiddenInput()->label('') ?>
        
        <?php ActiveForm::end(); ?>
    </div>
</div>