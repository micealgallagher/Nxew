<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */


?>
<div class="account-form">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">
                Profile
            </h3>
                <?php
                    if ( $model->isNewRecord ) {
                        $form = ActiveForm::begin(['id' => 'add-account', 'action' => Yii::$app->getUrlManager()->createUrl('account/create')]);
                    } else {
                        $form = ActiveForm::begin(['id' => 'update-account']);
                    }

                    $options = ['class' => 'btn pull-right accept-bio-button'];
                    $icon = Html::tag('i', '', $options);

                    echo Html::submitButton($icon, ['class' => 'pull-right accept-bio-button']);
                ?>
        </div>
        <div class="panel-body">
            
            <?= $form->field($user, 'artist')->textInput(['placeholder' => 'ARTIST', 'class' => 'form-control login-password']) ?>
            <?= $form->field($model, 'bio')->textarea(['placeholder' => 'BIO', 'rows' => 6, 'class' => 'form-control login-password']) ?>
            <?= $form->field($model, 'website')->textInput(['placeholder' => 'WEBSITE', 'class' => 'form-control login-password']) ?>
            <?= $form->field($model, 'facebook')->textInput(['placeholder' => 'FACEBOOK', 'class' => 'form-control login-password']) ?>
            <?= $form->field($model, 'twitter')->textInput(['placeholder' => 'TWITTER', 'class' => 'form-control login-password']) ?>
            <?= $form->field($model, 'soundcloud')->textInput(['placeholder' => 'SOUNDCLOUD', 'class' => 'form-control login-password']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

        <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">
                <a name="playlist" id="playlist">Playlist</a>
            </h3>
                <?php
                    if ( $model->isNewRecord ) {
                        $form = ActiveForm::begin(['id' => 'add-account', 'action' => Yii::$app->getUrlManager()->createUrl('account/create')]);
                    } else {
                        $form = ActiveForm::begin(['id' => 'update-account']);
                    }

                    $options = ['class' => 'btn pull-right accept-bio-button'];
                    $icon = Html::tag('i', '', $options);

                    echo Html::submitButton($icon, ['class' => 'pull-right accept-bio-button']);
                ?>
        </div>
        <div class="panel-body" style="max-height: 103px">
            
            <?= $form->field($model, 'playlist_name')->textInput(['placeholder' => 'PLAYLIST NAME', 'class' => 'form-control login-password']) ?>

            <?= $form->field($user, 'artist')->hiddenInput()->label('') ?>
            <?= $form->field($model, 'bio')->hiddenInput()->label('') ?>
            <?= $form->field($model, 'website')->hiddenInput()->label('') ?>
            <?= $form->field($model, 'facebook')->hiddenInput()->label('') ?>
            <?= $form->field($model, 'twitter')->hiddenInput()->label('') ?>
            <?= $form->field($model, 'soundcloud')->hiddenInput()->label('') ?>
            
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <?php
        if ( $user->isAdministrator() ) {
            echo $this->render('_contribute', [
                'contributeSetting' => $contributeSetting,
            ]);
        }
    ?>
       
</div>
