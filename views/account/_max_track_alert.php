<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">No more tracks for you!</h4>
            </div>
            <div class="modal-body">
                <?= 'Sorry, but you are not allowed to add any more than ' . (isset($playlistTracks) ? sizeof($playlistTracks) : -1) . ' tracks' ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">:( OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>