<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */


?>
<div class="panel panel-success panel-shadow profile-panel">
    <div class="panel-heading acount-panel-heading">
        <h3 class="account-panel-title" style="height: 30px">
            <span class="shadow-text" style="font-weight: bold;">
                <?= Html::encode($user->getArtistName()) ?>
            </span>

            
                                    
            <span style="display: block; float left; font-size:small; padding-top: 3px; font-weight: normal">
                <?= Html::encode("Ottawa, ON") ?>
            </span>
        </h3>
        <?php

            if ( $doesUserOwnPlaylist ) {
                $options = ['class' => 'btn pull-right edit-bio-button'];
                $icon = Html::tag('i', '', $options);

                echo Html::a($icon, ['/account/update', 'id' => $account->id]);
            }
       ?>
    </div>
    <div class="panel-body account-bio" style="font-weight: normal">
        <?= Html::encode($account->bio) ?>
    </div>
    <div class="panel-body social-media-icons">
        <?php
            if ( $account->hasAnySocialItems() ) {

                echo "<hr/>";

                if (strlen($account->website) > 0) {
                    $options = ['class' => 'fa fa-home fa-3x social-icon'];
                    $icon = Html::tag('i', '', $options);

                    echo Html::a($icon, $account->website);
                }

                if (strlen($account->twitter) > 0) {
                    $options = ['class' => 'fa fa-twitter-square fa-3x social-icon'];
                    $icon = Html::tag('i', '', $options);

                    echo Html::a($icon, $account->twitter);
                }

                if (strlen($account->facebook) > 0) {
                    $options = ['class' => 'fa fa-facebook-square fa-3x social-icon'];
                    $icon = Html::tag('i', '', $options);

                    echo Html::a($icon, $account->facebook);
                }

                if (strlen($account->soundcloud) > 0) {
                    $options = ['class' => 'fa fa-soundcloud fa-3x social-icon'];
                    $icon = Html::tag('i', '', $options);

                    echo Html::a($icon, $account->soundcloud);
                }
            }
        ?>

    </div>
</div>