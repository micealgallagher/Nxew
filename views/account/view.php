<?php


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'View Account';
?>
<div class="account-view">

    <div class="row account-view-row">
        <div class="col-md-4 col-lg-4 col-sm-12">
            <?= $this->render('_profile', [
                'account' => $account,
                'user' => $user,
                'doesUserOwnPlaylist' => $doesUserOwnPlaylist
            ]) ?>
        </div>
        <div class="col-md-8 col-lg-8 col-sm-12">
            <?= $this->render('_playlist', [
                'account' => $account,
                'user' => $user,
                'addSoundCloud' => $addSoundCloud,
                'playlistTracks' => $playlistTracks,
                'maxNumberOfTracksReached' => $maxNumberOfTracksReached,
                'doesUserOwnPlaylist' => $doesUserOwnPlaylist
            ]) ?>
        </div>
    </div>
</div>