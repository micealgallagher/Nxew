<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\common\Constant;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="shortcut icon" href="<?= Yii::$app->request->baseUrl . '/favicon.ico' ?>" type="image/x-icon">
        <link href='https://fonts.googleapis.com/css?family=Roboto:500,400,700' rel='stylesheet' type='text/css'>    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
            'brandLabel' => Html::img(Yii::$app->request->baseUrl . '/img/nxewlogo.png', ['height' => '60px']),
            'brandUrl' => Yii::$app->homeUrl,
            'brandOptions' => [
                'style' => 'padding: 0px;'
            ],
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
                'style' => 'background-color: #4b4b4b; height: 60px;'
            ],
        ]);

        $allMenuItems = [];
        $usersMenuItem = [];
        $loginMenuItem = [];

        if ( Yii::$app->user->isGuest ) {
            $loginMenuItem = ['label' => 'Login', 'url' => ['/login/login']];
            $contributeMenuItem = ['label' => 'Contribute', 'url' => ['/login/contribute']];
        } else {

            $loginMenuItem = [
                'label' => 'Logout (' . Yii::$app->user->identity->email . ')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']
            ];

            // Add menu items that are only available to ADMIN users
            $userType = Yii::$app->session->get(Constant::USER_TYPE);
            if ( Constant::USER_TYPE_ADMIN === $userType) {
                $usersMenuItem = [
                    'label' => 'Users ',
                    'items' => [
                        ['url' => ['user/create'], 'label' => 'Add User'],
                        ['url' => ['user/index'], 'label' => 'List User', 'icon' => 'arrow-right', 'content' => $content],
                    ]
                ];
            }

            $accountMenuItem = [
                'label' => 'Account ',
                'url' => ['account/view']
            ];
        }

        if ( isset($contributeMenuItem) ) {
            array_push($allMenuItems, $contributeMenuItem);
        }

        if ( isset($accountMenuItem) ) {
            array_push($allMenuItems, $accountMenuItem);
        }
        if ( $usersMenuItem ) {
            array_push($allMenuItems, $usersMenuItem);
        }
        if ( $loginMenuItem ) {
            array_push($allMenuItems, $loginMenuItem);
        }

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $allMenuItems,
        ]);



        NavBar::end();
        ?>

        <div class="container">
            <?= $content ?>

        </div>
    </div>

    <div class="navbar navbar-fixed-bottom" style="background-color: #4b4b4b">
        <footer  style="background: #4b4b4b; padding-top: 0px" class="footer">
            
            <div class="col-lg-8 col-lg-offset-2">
                <div id="player">
                </div>
            </div>

        </footer>
    </div>

    <?php $this->endBody() ?>
    

    <script>
            if ( trackUrls != undefined ) {
            (function() {

            }());

            ToneDenReady = window.ToneDenReady || [];
            ToneDenReady.push(function() {
                ToneDen.configure({
                    soundcloudConsumerKey: '75e6c4a9b578f90c37c810f32675f126'
                });
                // This is where all the action happens:
                ToneDen.player.create({
                    dom: "#player",
                    eq: "waves",
                    skin: "dark",
                    mini: true,
                    urls: trackUrls,
                    debug: true
                });
            });
        }
    </script>





    </body>
    </html>
<?php $this->endPage() ?>