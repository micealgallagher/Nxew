<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Password reset';
?>

<div class="row">
    <?php
        $showResetFuncitonality = true;
        if ( isset($foundUser) ) {
            if ( $foundUser) { 
                    $showResetFuncitonality = false;    
                ?>
                <div class="password-reset-panel-success col-xs-12 col-lg-8 col-lg-offset-2" style="margin-top: 100px; border: 0px solid black; max-height: 380px">
                    <div class="col-xs-12 col-lg-12">
                        <div class="panel">
                            <h2 class="panel-body title text-center">We found you!</h2>
                            <div class="panel-body contribute-text text-center">
                                <p>
                                    An email is on it's way with a password reset link.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
           <?php
            } else {
            ?>
                <div class="password-reset-panel-fail col-xs-12 col-lg-8 col-lg-offset-2" style="margin-top: 100px; border: 0px solid black; max-height: 380px">
                    <div class="col-xs-12 col-lg-12">
                        <div class="panel">
                            <h2 class="panel-body title text-center">Not Found!</h2>
                            <div class="panel-body contribute-text text-center">
                                <p>
                                    We are unable to find any record of that email address.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    ?>
</div>
<?php
    if ( $showResetFuncitonality ) {
?>
    <div class="row center-block" >
        <div class="col-xs-12 col-lg-4 col-lg-offset-4" style="margin-top: 100px">
            <div class="panel">
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(['id' => 'request-password-reset']); ?>
                    <?= $form->field($model, 'email')->textInput(['placeholder' => 'EMAIL', 'class' => 'form-control login-password'])->label('') ?>
                    <div class="form-group" style="text-align:center">
                        <?= Html::submitButton('RESET MY PASSWORD', ['class' => 'btn login-button', 'name' => 'add-user-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
<?php
    }
?>
