<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
?>

<?php
Yii::info('MGDEV - Password rest is: ' . isset($passwordReset) ? $passwordReset : false);
if ( isset($passwordReset) ? $passwordReset : false ) {
?>
    <div class="row center-block">
        <div class="col-xs-4 col-xs-offset-4 col-lg-41">
            <?php
                $options = ['class' =>'alert alert-success'];
                echo Html::tag('div', '<strong>Updated!</strong> Password updated successfully.', $options);
            ?>
        </div>
    </div>
<?php
}
?>


<div class="row center-block" >
    <div class="col-xs-12 col-lg-4 col-lg-offset-4" style="margin-top: 100px">
        <div class="panel">
            <div class="panel-body">
                partial render
            <?= $this->render('_form') ?>

            </div>
        </div>
    </div>
</div>