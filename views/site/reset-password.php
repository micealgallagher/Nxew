<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'New Password';
?>

<div class="user-form">

    <div class="row center-block" >
        <div class="col-xs-4 col-xs-offset-4 col-lg-4 col-lg-offset-4" style="margin-top: 100px">
            <div class="panel">
                <div class="panel-body">

                    <?php
                        if ( (isset($passwordUpdatedSuccessfully) ? $passwordUpdatedSuccessfully : false) ) {
                            $options = ['class' =>'alert alert-success'];
                            echo Html::tag('div', '<strong>Updated!</strong> Password updated successfully.', $options);
                        }

                        $form = ActiveForm::begin(['id' => 'reset-password', 'action' => Yii::$app->getUrlManager()->createUrl(['site/reset-password', 'token' => $securityForm->token])]);

                        echo $form->field($securityForm, 'newPassword')->passwordInput(['placeholder' => 'NEW PASSWORD', 'class' => 'form-control login-password'])->label('');
                        echo $form->field($securityForm, 'newPasswordAgain')->passwordInput(['placeholder' => 'REPEAT NEW PASSWORD', 'class' => 'form-control login-password'])->label('');

                    ?>
                        <div class="form-group" style="text-align:center">
                            <?= Html::submitButton('I\'LL REMEBER IT THIS TIME', ['class' => 'btn login-button']) ?>
                        </div>
                    <?php
                        ActiveForm::end();
                    ?>

                </div>
            </div>
        </div>
    </div>
    

</div>
