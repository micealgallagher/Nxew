<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Random Playlist';
?>
<div class="account-view">

    <div class="row account-view-row">
        <div class="col-md-4 col-lg-4 col-sm-5">
            <?= $this->render('../account/_profile', [
                'account' => $account,
                'user' => $user,
                'doesUserOwnPlaylist' => $doesUserOwnPlaylist
            ]) ?>
        </div>
        <div class="col-md-8 col-lg-8 col-sm-7">
            <?= $this->render('../account/_playlist', [
                'account' => $account,
                'user' => $user,
                'addSoundCloud' => null,
                'playlistTracks' => $playlistTracks,
                'maxNumberOfTracksReached' => null,
                'doesUserOwnPlaylist' => $doesUserOwnPlaylist
            ]) ?>
        </div>
    </div>
</div>