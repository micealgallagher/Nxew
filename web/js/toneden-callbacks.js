wasSkipTrackCalled = false

function showTrackAsCurrentlyPlaying(trackIndex) {
	console.log("MGDEV - showTrackAsCurrentlyPlaying - " + trackIndex);
	// Remove the current track playing css rule
	$('[class*="track-playing"]').removeClass('track-playing');  
	// Remove the current pause overlay
    $('[class*="pause-overlay"]')
    	.removeClass("pause-overlay")
    	.addClass("no-pause-overlay");

    // Add the coverart overlay to the track
    $('div[coverart-index="' + trackIndex + '"]')
            .removeClass('no-pause-overlay')
            .addClass('pause-overlay');
    // Add the track playing css rule to the row
    $('div[track-container-id="' + trackIndex + '"]')
            .addClass('track-playing');

	$pauseOverlay = $('div[coverart-index="' + trackIndex + '"]');
	$pauseOverlay.click(function () {
		ToneDen.player.getInstanceByDom("#player").togglePause(true)
		$(this).removeClass("pause-overlay").addClass("no-pause-overlay");
	});
}

function addRemovePauseOverlay(trackIndex) {
	coverartOverlay = $('div[coverart-index=' + trackIndex + ']')

	var isTrackPlaying = coverartOverlay.hasClass('pause-overlay');

	if (isTrackPlaying) {
		coverartOverlay	.removeClass("pause-overlay").addClass("no-pause-overlay");
		$('div[track-container-id="' + trackIndex + '"]').removeClass('track-playing');
	} else {
		$('div[track-container-id="' + trackIndex + '"]').addClass('track-playing');
		coverartOverlay.removeClass("no-pause-overlay").addClass("pause-overlay");

		coverartOverlay.click(function() {
			ToneDen.player.getInstanceByDom("#player").togglePause(true);
			addRemovePauseOverlay();
		});
	}
}

function getTrackIndexAsInt(trackIndex) {
	return parseInt(trackIndex.split("_")[1]);
}

function playButtonClicked(trackIndex) {
	trackIndex = getTrackIndexAsInt(trackIndex) - 1;
	console.log("MGDEV - playButtonClicked - trackIndex=" + trackIndex);
	showTrackAsCurrentlyPlaying(trackIndex);
}

function nextButtonClicked(trackIndex) {
	trackIndex = getTrackIndexAsInt(trackIndex);
	console.log("MGDEV - nextButtonClicked - trackIndex=" + trackIndex);
	showTrackAsCurrentlyPlaying(trackIndex);
}

function prevButtonClicked(trackIndex) {
	trackIndex = getTrackIndexAsInt(trackIndex) - 2;
	console.log("MGDEV - prevButtonClicked - trackIndex=" + trackIndex);
	showTrackAsCurrentlyPlaying(trackIndex);
}

function beginButtonClicked(trackIndex) {
	console.log("MGDEV - beginButtonClicked - trackIndex=" + trackIndex);

	showTrackAsCurrentlyPlaying(trackIndex);
}