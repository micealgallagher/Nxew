<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */
$this->title = 'Account Created';
?>
<center>
    <?php
        $imgOptions = ['height' => '80px', 'alt' => ' Nxew Logo'];
        $imgUrl = Yii::$app->urlManager->hostInfo . Yii::$app->urlManager->baseUrl . '/img/nxewlogo.png';

        echo Html::img($imgUrl, $imgOptions)
    ?>
    <hr width="50%"/>
    <h1><?= Html::encode($this->title) ?></h1>
    <h3>Your Password</h3>
    <?= $password ?>
    <hr width="50%"/>
    <i>The Sign Painter</i>
</center>
