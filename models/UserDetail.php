<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class UserDetail extends Model
{
    public $userId;
    public $artist;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // artist is a required field
            [['artist'], 'required'],
        ];
    }
}
